

function show() {
    var sangue = document.getElementById("sangueCheck");
    var urina = document.getElementById("urinaCheck");
    var medula = document.getElementById("medulaOsseaCheck");

    var eritrogramaCheck = document.getElementById("eritrogramaCheck");
    var leucogramaCheck = document.getElementById("leucogramaCheck");
    var mielogramaCheck = document.getElementById("mielogramaCheck");
    var bioquimicoCheck = document.getElementById("bioquimicoCheck");
    var upcCheck = document.getElementById("upcCheck");

    var eritrograma = document.getElementById("eritrograma");
    var leucograma = document.getElementById("leucograma");
    var bioquimico = document.getElementById("bioquimico");
    var mielograma = document.getElementById("mielograma");
    var upc = document.getElementById("upc");


    if (sangue.checked) {

        eritrogramaCheck.disabled = false;
        leucogramaCheck.disabled = false;
    }
    else {
        eritrogramaCheck.disabled = true;
        leucogramaCheck.disabled = true;
    }

    if (urina.checked) {
        bioquimicoCheck.disabled = false;
        upcCheck.disabled = false;
    }

    else {
        bioquimicoCheck.disabled = true;
        upcCheck.disabled = true;
    }


    if (medula.checked)
        mielogramaCheck.disabled = false;

    else
        mielogramaCheck.disabled = true;

    if (eritrogramaCheck.checked)
        eritrograma.style.display = "block";
    else
        eritrograma.style.display = "none";

    if (leucogramaCheck.checked)
        leucograma.style.display = "block";
    else
        leucograma.style.display = "none";

    if (mielogramaCheck.checked)
        mielograma.style.display = "block";
    else
        mielograma.style.display = "none";

    if (bioquimicoCheck.checked)
        bioquimico.style.display = "block";
    else
        bioquimico.style.display = "none";

    if (upcCheck.checked)
        upc.style.display = "block";
    else
        upc.style.display = "none";

}   